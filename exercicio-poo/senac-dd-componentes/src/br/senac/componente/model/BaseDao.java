
package br.senac.componente.model;


public interface BaseDao<A, B> {
    public A getPorId (B id);
    public boolean excluir (B id);
    public boolean alterar (A object );
    public B inserir (A object);
    
    
}
