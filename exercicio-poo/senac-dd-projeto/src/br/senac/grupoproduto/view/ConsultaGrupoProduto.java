package br.senac.grupoproduto.view;

import br.senac.produto.model.GrupoProduto;
import br.senacgrupoproduto.model.GrupoProdutoDao;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class ConsultaGrupoProduto extends javax.swing.JFrame {

    public GrupoProdutoDao dao = new GrupoProdutoDao();
    String[] colunas = new String[]{"Código", "Descrição", "Tipo"};

    public ConsultaGrupoProduto() {
        initComponents();
        buscarDados();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jToolBar2 = new javax.swing.JToolBar();
        btNovo = new javax.swing.JButton();
        btEditar = new javax.swing.JButton();
        btExcluir = new javax.swing.JButton();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 0));
        btFechar = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbGrupoProduto = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        lbDescricao = new javax.swing.JLabel();
        txDescricao = new javax.swing.JTextField();
        btBuscar = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        mnNovo = new javax.swing.JMenuItem();
        mnEditar = new javax.swing.JMenuItem();
        mnExcluir = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        mnImprimir = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setName("frame"); // NOI18N

        jToolBar2.setRollover(true);

        btNovo.setText("Novo");
        btNovo.setFocusable(false);
        btNovo.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btNovo.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btNovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btNovoActionPerformed(evt);
            }
        });
        jToolBar2.add(btNovo);

        btEditar.setText("Editar");
        btEditar.setFocusable(false);
        btEditar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btEditar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btEditarActionPerformed(evt);
            }
        });
        jToolBar2.add(btEditar);

        btExcluir.setText("Excluir");
        btExcluir.setFocusable(false);
        btExcluir.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btExcluir.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btExcluirActionPerformed(evt);
            }
        });
        jToolBar2.add(btExcluir);
        jToolBar2.add(filler1);

        btFechar.setText("Fechar");
        btFechar.setFocusable(false);
        btFechar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btFechar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jToolBar2.add(btFechar);

        getContentPane().add(jToolBar2, java.awt.BorderLayout.PAGE_START);

        tbGrupoProduto.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Código", "Descrição", "Tipo Produto"
            }
        ));
        jScrollPane1.setViewportView(tbGrupoProduto);

        lbDescricao.setText("Descrição:");

        btBuscar.setText("Buscar");
        btBuscar.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btBuscarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbDescricao)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txDescricao, javax.swing.GroupLayout.PREFERRED_SIZE, 542, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(118, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbDescricao, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txDescricao, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(52, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addGap(26, 26, 26))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 537, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);

        jMenu1.setText("Manutenção");

        mnNovo.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        mnNovo.setText("Novo");
        jMenu1.add(mnNovo);

        mnEditar.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        mnEditar.setText("Editar");
        jMenu1.add(mnEditar);

        mnExcluir.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_X, java.awt.event.InputEvent.CTRL_MASK));
        mnExcluir.setText("Excluir");
        mnExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnExcluirActionPerformed(evt);
            }
        });
        jMenu1.add(mnExcluir);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Relatórios");

        mnImprimir.setText("Imprimir");
        jMenu2.add(mnImprimir);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        getAccessibleContext().setAccessibleDescription("");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public void buscarDados() {
        DefaultTableModel model = new DefaultTableModel(colunas, 0);
        List<GrupoProduto> lista = dao.listarPorNome(txDescricao.getText());

        for (GrupoProduto grupo : lista) {
            Object[] dados = new Object[3];
            String id = grupo.getIdGrupoProduto().toString();
            if (id.length() == 1) {
                id = "0" + id;
            }

            dados[0] = grupo.getIdGrupoProduto();
            dados[1] = grupo.getNomeGrupoProduto();
            dados[2] = grupo.getTipoProduto();
            model.addRow(dados);

        }
        tbGrupoProduto.setAutoCreateRowSorter(true);
        tbGrupoProduto.setModel(model);
        tbGrupoProduto.getColumnModel().getColumn(0).setPreferredWidth(20);
        tbGrupoProduto.getColumnModel().getColumn(1).setPreferredWidth(50);
        tbGrupoProduto.getColumnModel().getColumn(2).setPreferredWidth(30);

    }
    private void btNovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btNovoActionPerformed
        CadGrupoProduto cad = new CadGrupoProduto();
        cad.incluir();
        buscarDados();
    }//GEN-LAST:event_btNovoActionPerformed

    private void btBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btBuscarActionPerformed
        if (txDescricao.getText().trim().equals("")) {
            JOptionPane.showMessageDialog(this, "Nenhum registro informado", "Aviso", JOptionPane.WARNING_MESSAGE);
            return;
        }
        buscarDados();


    }//GEN-LAST:event_btBuscarActionPerformed

    private void mnExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnExcluirActionPerformed
        if (tbGrupoProduto.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(this, "Nenhum registro selecionado.", "Erro", JOptionPane.WARNING_MESSAGE);

            return;
        }
        Long id = (Long) tbGrupoProduto.getValueAt(tbGrupoProduto.getSelectedRow(), 0);

        int retorno = JOptionPane.showConfirmDialog(this, "Deseja realmente excluir o produto?",
                "Atenção", JOptionPane.OK_CANCEL_OPTION);

        if (retorno == JOptionPane.OK_OPTION) {
            GrupoProdutoDao dao = new GrupoProdutoDao();
            try {//tentar executar
                dao.excluir(id);
                JOptionPane.showMessageDialog(this, "Exclusão finalizada com sucesso.", "Sucesso", JOptionPane.INFORMATION_MESSAGE);
                buscarDados();
            } catch (Exception e) {//Se ocorrer erro, vou tratar.
                //Se entrar aqui é porque ocorreu algum erro(exceção)
                JOptionPane.showMessageDialog(this, "Erro ao tentar excluir: " + e.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_mnExcluirActionPerformed

    private void btEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btEditarActionPerformed
        if (tbGrupoProduto.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(this, "Nenhum registro selecionado.", "Erro", JOptionPane.WARNING_MESSAGE);

            return;
        }
        CadGrupoProduto cad = new CadGrupoProduto();
        Long id = (Long) tbGrupoProduto.getValueAt(tbGrupoProduto.getSelectedRow(), 0);
        cad.editar(id);
        buscarDados();

    }//GEN-LAST:event_btEditarActionPerformed

    private void btExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btExcluirActionPerformed
         if (tbGrupoProduto.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(this, "Nenhum registro selecionado.", "Erro", JOptionPane.WARNING_MESSAGE);

            return;
        }
        Long id = (Long) tbGrupoProduto.getValueAt(tbGrupoProduto.getSelectedRow(), 0);
        dao.excluir(id);
        buscarDados();
    }//GEN-LAST:event_btExcluirActionPerformed

    public static void main(String args[]) {

        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ConsultaGrupoProduto.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ConsultaGrupoProduto.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ConsultaGrupoProduto.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ConsultaGrupoProduto.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ConsultaGrupoProduto().setVisible(true);
            }
        });
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btBuscar;
    private javax.swing.JButton btEditar;
    private javax.swing.JButton btExcluir;
    private javax.swing.JButton btFechar;
    private javax.swing.JButton btNovo;
    private javax.swing.Box.Filler filler1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JToolBar jToolBar2;
    private javax.swing.JLabel lbDescricao;
    private javax.swing.JMenuItem mnEditar;
    private javax.swing.JMenuItem mnExcluir;
    private javax.swing.JMenuItem mnImprimir;
    private javax.swing.JMenuItem mnNovo;
    private javax.swing.JTable tbGrupoProduto;
    private javax.swing.JTextField txDescricao;
    // End of variables declaration//GEN-END:variables
}
