package br.senac.grupoproduto.view;

import br.senac.produto.model.GrupoProduto;
import br.senac.produto.model.TipoProduto;
import br.senacgrupoproduto.model.GrupoProdutoDao;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class CadGrupoProduto extends JDialog {
    
    public static void main(String[] args) {
        CadGrupoProduto cad = new CadGrupoProduto();
    }
    private JTextField txCod = new JTextField();
    private JTextField txNome = new JTextField();
    private JButton btCancelar = new JButton("Cancelar");
    private JButton btGravar = new JButton("Gravar");
    private JLabel lbCodigo = new JLabel("Codigo: ");
    private JLabel lbNome = new JLabel("Nome: ");
    private JLabel lbTipo = new JLabel("Tipo: ");
    private JRadioButton rbServico = new JRadioButton("Serviço");
    private JRadioButton rbMatPrima = new JRadioButton("Mateéria Prima");
    private JRadioButton rbMercadoria = new JRadioButton("Mercadoria");
    
    public CadGrupoProduto() {
        configuraTela();
        criaBotao();
        criaLabel();
        
    }
    
    public void criaBotao() {
        
        JPanel pnBotoes = new JPanel();
        pnBotoes.setLayout(new FlowLayout());
        pnBotoes.add(btGravar);
        pnBotoes.add(btCancelar);
        pnBotoes.setSize(300, 50);
        add(pnBotoes, BorderLayout.SOUTH);
        
        btGravar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                gravar();
            }
        });
        
    }
    
    public void configuraTela() {
        setTitle("Cadastro Grupo Produto");
        setSize(500, 150);
        setLocationRelativeTo(null);
        setVisible(true);
        setLayout(new BorderLayout());
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }
    
    public void criaLabel() {
        
        JPanel pnDados = new JPanel();
        pnDados.setLayout(new GridLayout(3, 2));
        add(pnDados, BorderLayout.CENTER);
        pnDados.add(lbCodigo);
        pnDados.add(txCod);
        pnDados.add(lbNome);
        pnDados.add(txNome);
        pnDados.add(lbTipo);
        JPanel pnTipo = new JPanel();
        pnTipo.setLayout(new GridLayout(1, 3));
        pnTipo.add(rbMercadoria);
        pnTipo.add(rbMatPrima);
        pnTipo.add(rbServico);
        pnDados.add(pnTipo);
        lbCodigo.setHorizontalAlignment(JLabel.RIGHT);
        lbNome.setHorizontalAlignment(JLabel.RIGHT);
        lbTipo.setHorizontalAlignment(JLabel.RIGHT);
        
        txCod.setEditable(false);
        
        ButtonGroup bgTipo = new ButtonGroup();
        bgTipo.add(rbServico);
        bgTipo.add(rbMatPrima);
        bgTipo.add(rbMercadoria);
        
    }
    
    public void gravar() {
        //DAO = Camada Persistência = Data Acess Object
        GrupoProdutoDao dao = new GrupoProdutoDao();

        // Data Source. GrupoProduto = Been
        GrupoProduto grupoProduto = new GrupoProduto();
        //DataBinding
        if (txNome.getText() == null
                || txNome.getText().trim().equals("")) {
            JOptionPane.showMessageDialog(this, "Nome inválido.", "Erro", JOptionPane.ERROR_MESSAGE);
            txNome.requestFocus();
            
            return;
        }
        grupoProduto.setNomeGrupoProduto(txNome.getText());
        if (rbMatPrima.isSelected() == false
                && rbServico.isSelected() == false
                && rbMercadoria.isSelected() == false) {
            JOptionPane.showMessageDialog(this, "Nenhum tipo informado.", "Erro", JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        List<GrupoProduto> listaGrupProd = dao.listarPorNome(txNome.getText());
        for (GrupoProduto grupProd : listaGrupProd) {
            if (grupProd.getNomeGrupoProduto().equalsIgnoreCase(txNome.getText())
                    && grupProd.getIdGrupoProduto().equals(idGrupoProduto) == false) {
                JOptionPane.showConfirmDialog(this, "Nome já cadastrado.", "Erro", JOptionPane.ERROR_MESSAGE);
                return;
            }
            
        }
        
        int resp = JOptionPane.showConfirmDialog(this, "Deseja realmente salvar este produto?",
                "Escolha uma opção", JOptionPane.OK_CANCEL_OPTION);
        
        if (resp != JOptionPane.OK_OPTION) {
            return;
        }
        
        if (rbMatPrima.isSelected()) {
            grupoProduto.setTipoProduto(TipoProduto.MATERIA_PRIMA);
        } else if (rbServico.isSelected()) {
            grupoProduto.setTipoProduto(TipoProduto.SERVICO);
        } else if (rbMercadoria.isSelected()) {
            grupoProduto.setTipoProduto(TipoProduto.MERCADORIA);
        }
        
        if (idGrupoProduto == null) {
            
            Long idNovo = dao.inserir(grupoProduto);
            JOptionPane.showMessageDialog(this, "Grupo produto inserido com sucesso.", "Informação", JOptionPane.INFORMATION_MESSAGE);
        } else {
            grupoProduto.setIdGrupoProduto(idGrupoProduto);
            dao.alterar(grupoProduto);
            JOptionPane.showMessageDialog(this, "Grupo produto alterado com sucesso.", "Informação", JOptionPane.INFORMATION_MESSAGE);
            
        }
        
    }
    private Long idGrupoProduto = null;
    private String resp = null;
    
    public void editar(Long idGrupoProduto) {
        this.idGrupoProduto = idGrupoProduto;
        
        if (idGrupoProduto != null) {
            GrupoProdutoDao dao = new GrupoProdutoDao();
            GrupoProduto grupoProduto = null;
            grupoProduto = dao.getPorId(idGrupoProduto);
            
            txNome.setText(grupoProduto.getNomeGrupoProduto());
            txCod.setText(Long.toString(grupoProduto.getIdGrupoProduto()));
            if (grupoProduto.getTipoProduto() == TipoProduto.MATERIA_PRIMA) {
                rbMatPrima.setSelected(true);
            }
            if (grupoProduto.getTipoProduto() == TipoProduto.SERVICO) {
                rbServico.setSelected(true);
            }
            if (grupoProduto.getTipoProduto() == TipoProduto.MERCADORIA) {
                rbMercadoria.setSelected(true);
                
            }
        }
        
    }

    public void incluir() {
        setVisible(true);
    }
    public void excluir(Long idGrupoProduto) {
        this.idGrupoProduto = idGrupoProduto;
        
        if (idGrupoProduto != null) {
            GrupoProdutoDao dao = new GrupoProdutoDao();
            GrupoProduto grupoProduto = null;
            grupoProduto = dao.getPorId(idGrupoProduto);
            
       
            if (grupoProduto.getTipoProduto() == TipoProduto.MATERIA_PRIMA) {
                rbMatPrima.setVisible(false);
            }
            if (grupoProduto.getTipoProduto() == TipoProduto.SERVICO) {
                rbServico.setVisible(false);
            }
            if (grupoProduto.getTipoProduto() == TipoProduto.MERCADORIA) {
                rbMercadoria.setVisible(false);
                
            }
        }
    }
    
}
