
package br.senac.produto.model;


public class Servico extends Produto {
    private Long idServico;
    private Float percISS;

    public Long getIdServico() {
        return idServico;
    }

    public void setIdServico(Long idServico) {
        this.idServico = idServico;
    }
    public Float getPercISS() {
        if (percISS == null)
            return 0.0F;
        return percISS;
    }

    public void setPercISS(Float percISS) {
        this.percISS = percISS;
    }
    
    @ Override
    
    public Float getTotalPercImposto (){
        return getPercICMS() + getPercISS();
    }
}
