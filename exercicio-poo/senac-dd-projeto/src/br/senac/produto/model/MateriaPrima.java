
package br.senac.produto.model;


public class MateriaPrima extends Produto {
    private Long idMateriaPrima;
    private Float percIPI;

    public Long getIdMateriaPrima() {
        return idMateriaPrima;
    }

    public void setIdMateriaPrima(Long idMateriaPrima) {
        this.idMateriaPrima = idMateriaPrima;
    }

    public Float getPercIPI() {
        if (percIPI == null)
            return 0.0F;
        
        return percIPI;
    }

    public void setPercIPI(Float percIPI) {
        this.percIPI = percIPI;
    }
    
    @ Override
    
    public Float getTotalPercImposto (){
        return getPercICMS() + getPercIPI();
    }
}
