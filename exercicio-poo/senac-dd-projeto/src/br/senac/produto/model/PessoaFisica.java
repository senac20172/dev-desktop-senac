package br.senac.produto.model;

import java.util.Date;

public class PessoaFisica {

    private Integer codigo;
    private String nome;
    private String sexo;
    private boolean pcd;
    private String deficiencia;
    private Date dataNascimento;
    private double renda;
    
    //bean  

    public PessoaFisica() {
    }

    public PessoaFisica(Integer codigo, String nome, String sexo, 
        boolean pcd, String deficiencia, Date dataNascimento, double renda) {
        this.codigo = codigo;
        this.nome = nome;
        this.sexo = sexo;
        this.pcd = pcd;
        this.deficiencia = deficiencia;
        this.dataNascimento = dataNascimento;
        this.renda = renda;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public boolean isPcd() {
        return pcd;
    }

    public void setPcd(boolean pcd) {
        this.pcd = pcd;
    }

    public String getDeficiencia() {
        return deficiencia;
    }

    public void setDeficiencia(String deficiencia) {
        this.deficiencia = deficiencia;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public double getRenda() {
        return renda;
    }

    public void setRenda(double renda) {
        this.renda = renda;
    }
    
    
    
    
}
