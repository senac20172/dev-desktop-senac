
package br.senac.produto.model;

public class Mercadoria extends Produto {
    private Long idMercadoria;
    private byte[] imagem;

    public Long getIdMercadoria() {
        return idMercadoria;
    }

    public void setIdMercadoria(Long idMercadoria) {
        this.idMercadoria = idMercadoria;
    }

    public byte[] getImagem() {
        return imagem;
    }

    public void setImagem(byte[] imagem) {
        this.imagem = imagem;
    }
   
    @ Override
    
    public Float getTotalPercImposto (){
        return getPercICMS();
    }
}
