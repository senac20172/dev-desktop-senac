
package br.senac.produto.model;

import java.util.ArrayList;
import java.util.Objects;


public class GrupoProduto {
    private Long idGrupoProduto;
    private String nomeGrupoProduto;
    private TipoProduto tipoProduto;

    public GrupoProduto(Long idGrupoProduto, String nomeGrupoProduto, TipoProduto tipoProduto) {
        this.idGrupoProduto = idGrupoProduto;
        this.nomeGrupoProduto = nomeGrupoProduto;
        this.tipoProduto = tipoProduto;
    }

    public GrupoProduto() {
    }
    
    

    public Long getIdGrupoProduto() {
        return idGrupoProduto;
    }

    public void setIdGrupoProduto(Long idGrupoProduto) {
        this.idGrupoProduto = idGrupoProduto;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + Objects.hashCode(this.idGrupoProduto);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GrupoProduto other = (GrupoProduto) obj;
        if (!Objects.equals(this.idGrupoProduto, other.idGrupoProduto)) {
            return false;
        }
        return true;
    }

    public String getNomeGrupoProduto() {
        return nomeGrupoProduto;
    }

    public void setNomeGrupoProduto(String nomeGrupoProduto) {
        this.nomeGrupoProduto = nomeGrupoProduto;
    }

    public TipoProduto getTipoProduto() {
        return tipoProduto;
    }

    public void setTipoProduto(TipoProduto tipoProduto) {
        this.tipoProduto = tipoProduto;
    }

    public void add(ArrayList<GrupoProduto> grupoProdutos) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
