package br.senac.produto.model;

import java.util.Date;

public abstract class Produto {

    private Long idProduto;
    private String nomeProduto;
    private String descricaoProduto;
    private Date dataCriacao;
    private Date dataAlteracao;
    private Float percICMS;
    private TipoProduto tipoProduto;
    private GrupoProduto grupoProduto;

    public TipoProduto getTipoProduto() {
        return tipoProduto;
    }

    public void setTipoProduto(TipoProduto tipoProduto) {
        this.tipoProduto = tipoProduto;
    }

    public GrupoProduto getGrupoProduto() {
        return grupoProduto;
    }

    public void setGrupoProduto(GrupoProduto grupoProduto) {
        this.grupoProduto = grupoProduto;
    }

    public Long getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(Long idProduto) {
        this.idProduto = idProduto;
    }

    public String getNomeProduto() {
        return nomeProduto;
    }

    public void setNomeProduto(String nomeProduto) {
        this.nomeProduto = nomeProduto;
    }

    public String getDescricaoProduto() {
        return descricaoProduto;
    }

    public void setDescricaoProduto(String descricaoProduto) {
        this.descricaoProduto = descricaoProduto;
    }

    public Date getDataCriacao() {
        return dataCriacao;
    }

    public void setDataCriacao(Date dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    public Date getDataAlteracao() {
        return dataAlteracao;
    }

    public void setDataAlteracao(Date dataAlteracao) {
        this.dataAlteracao = dataAlteracao;
    }

    public Float getPercICMS() {
        if (percICMS == null) {
            return 0F;
        }
        return percICMS;
    }

    public void setPercICMS(Float percICMS) {
        this.percICMS = percICMS;
    }

    public abstract Float getTotalPercImposto();

}
