
package br.senac.produto.model;

import br.senacgrupoproduto.model.GrupoProdutoDao;
import java.util.List;
public class TesteGrupoProdutoDao {
    public static void main(String[] args) {
        
        GrupoProdutoDao dao = new GrupoProdutoDao();
        GrupoProduto gp = new GrupoProduto();
        gp.setIdGrupoProduto(5L);
        gp.setNomeGrupoProduto("Cobre");
        gp.setTipoProduto(TipoProduto.MERCADORIA);
        dao.alterar(gp);
    
        
        List<GrupoProduto> lista = dao.listarPorNome("A");
        for(GrupoProduto grupProd : lista) {
            System.out.println(grupProd.getNomeGrupoProduto());
            
        }
        System.out.println("Hashset");
        dao.listarTodos();
    }
    
    
}
