
package br.senac.pessoafisica.dao;

import br.senac.produto.model.PessoaFisica;

public class PessoaFisicaDao {

    public void salvar(PessoaFisica pessoaFisica) {
        System.out.println("Codigo: " + pessoaFisica.getCodigo());
        System.out.println("Nome: " + pessoaFisica.getNome());
        System.out.println("Sexo: " + pessoaFisica.getSexo());
        System.out.println("PCD: " + pessoaFisica.isPcd());
        System.out.println("Tipo Def: " + pessoaFisica.getDeficiencia());
        System.out.println("Nascimento: " + pessoaFisica.getDataNascimento());
        System.out.println("Renda: " + pessoaFisica.getRenda());
    }
}
