/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.senac.usuario.model;

import br.senac.dd.BaseDAO;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Gerson
 */
public class UsuarioDAO implements BaseDAO<Usuario, Long> {

    private static List<Usuario> listaUsuario;

    public UsuarioDAO() {
        if (listaUsuario == null) {
            listaUsuario = new ArrayList<>();
            listaUsuario.add(new Usuario(1L, "marcos", "abc123", null, 5));
            listaUsuario.add(new Usuario(2L, "maria", "abc123", null, 2));
            listaUsuario.add(new Usuario(3L, "joao", "abc123", null, 3));
            listaUsuario.add(new Usuario(4L, "carla", "abc123", null, 4));
            listaUsuario.add(new Usuario(5L, "aliane", "abc123", null, 10));
            listaUsuario.add(new Usuario(6L, "fernanda", "abc123", new GregorianCalendar(2015, Calendar.JULY, 5).getTime(), 4));
            listaUsuario.add(new Usuario(7L, "fernando", "abc123", null, 1));
            listaUsuario.add(new Usuario(8L, "marcosb", "abc123", null, 1));
            listaUsuario.add(new Usuario(9L, "gv123", "abc123", new GregorianCalendar(2010, Calendar.JANUARY, 23).getTime(), 4));
            listaUsuario.add(new Usuario(10L, "m4355", "abc123", null, 7));
            listaUsuario.add(new Usuario(11L, "ivo44", "abc123", null, 7));
            listaUsuario.add(new Usuario(12L, "f111", "abc123", new GregorianCalendar(1999, Calendar.DECEMBER, 1).getTime(), 6));
            listaUsuario.add(new Usuario(15L, "willian", "abc123", new GregorianCalendar(2012, Calendar.APRIL, 10).getTime(), 8));
            listaUsuario.add(new Usuario(14L, "denis", "abc123", null, 9));
        }
    }

    @Override
    public Long inserir(Usuario usuarioNovo) {
        Long idNovo = 0L;
        for (int i = 0; i < listaUsuario.size(); i++) {
            if (listaUsuario.get(i).getIdUsuario() > idNovo) {
                idNovo = listaUsuario.get(i).getIdUsuario();
            }
        }
        idNovo++;
        usuarioNovo.setIdUsuario(idNovo);
        listaUsuario.add(usuarioNovo);
        return idNovo;
    }

    @Override
    public boolean alterar(Usuario usuarioAtualizar) {
        int i = 0;
        for (Usuario usuario : listaUsuario) {
            if (usuarioAtualizar.equals(usuario)) {
                listaUsuario.set(i, usuarioAtualizar);
                return true;
            }
            i++;
        }
        return false;
    }

    @Override
    public boolean excluir(Long id) {
        for (int i = 0; i < listaUsuario.size(); i++) {
            if (listaUsuario.get(i).getIdUsuario().equals(id)) {
                listaUsuario.remove(i);
                return true;
            }
        }
        return false;
    }

    @Override
    public Usuario getPorId(Long id) {
        for (int i = 0; i < listaUsuario.size(); i++) {
            if (listaUsuario.get(i).getIdUsuario().equals(id)) {
                return listaUsuario.get(i);
            }
        }
        return null;
    }

    public List<Usuario> listarPorLogin(String login) {
        List<Usuario> listaLogin = new LinkedList<>();
        for (Usuario usuario : listaUsuario) {
            if (login == null || login.trim().equals("")
                    || usuario.getLogin().toUpperCase().contains(login.toUpperCase())) {
                listaLogin.add(usuario);
            }
        }

        Collections.sort(listaLogin, (Usuario o1, Usuario o2) -> {
            int comp = o1.getLogin().compareToIgnoreCase(o2.getLogin());
            if (comp == 0) {// nomes iguais
                return o1.getIdUsuario().compareTo(o2.getIdUsuario());
            } else {
                return comp;
            }
        });
        return listaLogin;
    }

}
