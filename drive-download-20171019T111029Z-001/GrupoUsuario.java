/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.senac.usuario.model;

import java.util.Objects;

/**
 *
 * @author Gerson
 */
public class GrupoUsuario {
    
    private Integer idGrupoUsuario;
    
    private String nome;

    public GrupoUsuario() {
    }

    public GrupoUsuario(Integer idGrupoUsuario, String nome) {
        this.idGrupoUsuario = idGrupoUsuario;
        this.nome = nome;
    }
    
    
    

    public Integer getIdGrupoUsuario() {
        return idGrupoUsuario;
    }

    public void setIdGrupoUsuario(Integer idGrupoUsuario) {
        this.idGrupoUsuario = idGrupoUsuario;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public String toString() {
        return nome;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GrupoUsuario other = (GrupoUsuario) obj;
        if (!Objects.equals(this.idGrupoUsuario, other.idGrupoUsuario)) {
            return false;
        }
        return true;
    }
    
    
    
    
}
