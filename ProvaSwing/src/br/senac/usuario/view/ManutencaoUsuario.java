/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.senac.usuario.view;

import br.senac.usuario.model.GrupoUsuario;
import br.senac.usuario.model.GrupoUsuarioDAO;
import br.senac.usuario.model.Usuario;
import br.senac.usuario.model.UsuarioDAO;
import java.awt.Component;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author lucasarmindo
 */
public class ManutencaoUsuario extends javax.swing.JFrame {

    boolean verifica = true;

    /**
     * Creates new form ManutencaoUsuario
     */
    public ManutencaoUsuario() {

        initComponents();
        configuarTela(2);
        configuarTela(0);
        buscarDados();
        criarComboBox();
        listarDados();
        formataData();

    }
    private void limpaCampos(){
        txtData.setValue(null);
        txtLogin.setText("");
        txtPass.setText("");
    }
    private void formataData() {
        TableCellRenderer tableCellRenderer = new DefaultTableCellRenderer() {

            SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

            public Component getTableCellRendererComponent(JTable table,
                    Object value, boolean isSelected, boolean hasFocus,
                    int row, int column) {
                if (value instanceof Date) {
                    value = f.format(value);
                }
                return super.getTableCellRendererComponent(table, value, isSelected,
                        hasFocus, row, column);
            }
        };
        tbUsuarios.getColumnModel().getColumn(3).setCellRenderer(tableCellRenderer);
    }

    private void listarDados() {
        tbUsuarios.getSelectionModel().addListSelectionListener((ListSelectionEvent event) -> {
            if (event.getValueIsAdjusting()) {
                return;
            }

            if (tbUsuarios.getSelectedRow() != -1) {
                long id = (long) tbUsuarios.getValueAt(tbUsuarios.getSelectedRow(), 0);

                UsuarioDAO dao = new UsuarioDAO();

                Usuario userAlterar = dao.getPorId(id);

                if (verifica == false) {
                    txtLogin.setText(userAlterar.getLogin());
                    txtPass.setText(userAlterar.getSenha());
                    txtData.setValue(userAlterar.getDataExpiracao());
                    cbGrupo.setSelectedItem(userAlterar.getGrupoUsuario());
                }

            }
        });

    }

    private void criarComboBox() {
        GrupoUsuarioDAO daoGrupo = new GrupoUsuarioDAO();
        List<GrupoUsuario> lista = daoGrupo.listarTodos();
        DefaultComboBoxModel comboModel = new DefaultComboBoxModel(lista.toArray());
        cbGrupo.setModel(comboModel);
    }

    private void configuarTela(int estado) {
        switch (estado) {
            case 0:
                verifica = false;
                btnGravar.setVisible(false); //Esconde botão Gravar
                btnAlterar.setVisible(true); //Mostra botão Alterar
                btnExcluir.setVisible(true); //Mostra botão Excluir
                btnIncluir.setVisible(true); //Mostra botão incluir
                btnCancelar.setVisible(false); //Esconde botão cancelar
                txtData.setEnabled(false); //Desabilita acesso ao campo Data Expiração
                cbGrupo.setEnabled(false); //Desabilita o acesso ao campo ComboBox Grupo Usuário
                txtLogin.setEnabled(false); //Desabilita o acesso ao Login
                txtPass.setEnabled(false); //Desabilita o acesso a Senha
                break;

            case 1:
                verifica = true;
                btnCancelar.setVisible(true);
                btnGravar.setVisible(true);
                btnIncluir.setVisible(false);
                btnExcluir.setVisible(false);
                btnAlterar.setVisible(false);
                txtData.setEnabled(true); //Desabilita acesso ao campo Data Expiração
                cbGrupo.setEnabled(true); //Desabilita o acesso ao campo ComboBox Grupo Usuário
                txtLogin.setEnabled(true); //Desabilita o acesso ao Login
                txtPass.setEnabled(true); //Desabilita o acesso a Senha
                break;
            default:
                break;
        }
    }

    private void buscarDados() {
        DefaultTableModel model = (DefaultTableModel) tbUsuarios.getModel();
        model.setRowCount(0);
        UsuarioDAO dao = new UsuarioDAO();
        List<Usuario> lista = dao.listarPorLogin(null);

        for (Usuario grupo : lista) {
            Object[] dados = new Object[4];
            String id = grupo.getIdUsuario().toString();
            if (id.length() == 1) {
                id = "0" + id;
            }

            dados[0] = grupo.getIdUsuario();
            dados[1] = grupo.getLogin();
            dados[2] = grupo.getGrupoUsuario();
            dados[3] = grupo.getDataExpiracao();
            model.addRow(dados);

        }
        tbUsuarios.setAutoCreateRowSorter(true);
        tbUsuarios.setModel(model);
        tbUsuarios.getColumnModel().getColumn(0).setPreferredWidth(20);
        tbUsuarios.getColumnModel().getColumn(1).setPreferredWidth(50);
        tbUsuarios.getColumnModel().getColumn(2).setPreferredWidth(30);
        tbUsuarios.getColumnModel().getColumn(3).setPreferredWidth(30);
    }

    public void excluir() {
        if (tbUsuarios.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(this, "Nenhum registro selecionado.", "Erro", JOptionPane.WARNING_MESSAGE);

            return;
        }
        Long id = (Long) tbUsuarios.getValueAt(tbUsuarios.getSelectedRow(), 0);

        int retorno = JOptionPane.showConfirmDialog(this, "Deseja realmente excluir o produto?",
                "Atenção", JOptionPane.OK_CANCEL_OPTION);

        if (retorno == JOptionPane.OK_OPTION) {
            UsuarioDAO dao = new UsuarioDAO();
            try {//tentar executar
                dao.excluir(id);
                JOptionPane.showMessageDialog(this, "Exclusão finalizada com sucesso.", "Sucesso", JOptionPane.INFORMATION_MESSAGE);
                buscarDados();
            } catch (Exception e) {//Se ocorrer erro, vou tratar.
                //Se entrar aqui é porque ocorreu algum erro(exceção)

                JOptionPane.showMessageDialog(this, "Erro ao tentar excluir: " + e.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public void alterar() {
        if (tbUsuarios.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(this, "Nenhum registro selecionado.", "Erro", JOptionPane.WARNING_MESSAGE);
        } else {
            configuarTela(1);
            verifica = false;
        }
    }

    public void gravar() {

        Usuario user = new Usuario();
        if (txtLogin.getText().trim().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Login não informado.", "Erro", JOptionPane.ERROR_MESSAGE);
            return;
        } else {
            user.setLogin(txtLogin.getText());
        }

        if (txtPass.getPassword().length == 0) {
            JOptionPane.showMessageDialog(null, "Senha não informada.", "Erro", JOptionPane.ERROR_MESSAGE);
            return;
        } else {
            String senha = new String(txtPass.getPassword());
            user.setSenha(senha);
        }
        try {
            txtData.commitEdit();
            Date data = (java.util.Date) txtData.getValue();
            user.setDataExpiracao(data);
        } catch (ParseException ex) {
            Logger.getLogger(ManutencaoUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (cbGrupo.getSelectedItem() == null) {
            JOptionPane.showMessageDialog(null, "Grupo Acesso não informado.", "Erro", JOptionPane.ERROR_MESSAGE);
        } else {
            GrupoUsuario grupoUser = (GrupoUsuario) cbGrupo.getSelectedItem();
            user.setGrupoUsuario(grupoUser);
        }
        DefaultTableModel model = (DefaultTableModel) tbUsuarios.getModel();

        UsuarioDAO dao = new UsuarioDAO();

        if (verifica) {
            dao.inserir(user);

            buscarDados();
        } else {

            if (tbUsuarios.getSelectedRow() == -1) {
                JOptionPane.showMessageDialog(this, "Nenhum registro selecionado.", "Erro", JOptionPane.WARNING_MESSAGE);
            } else {
                user.setIdUsuario((long) tbUsuarios.getValueAt(tbUsuarios.getSelectedRow(), 0));
                dao.alterar(user);
                buscarDados();
            }
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        jToolBar1 = new javax.swing.JToolBar();
        btnIncluir = new javax.swing.JButton();
        btnAlterar = new javax.swing.JButton();
        btnExcluir = new javax.swing.JButton();
        btnGravar = new javax.swing.JButton();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 32767));
        btnCancelar = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbUsuarios = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtLogin = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtPass = new javax.swing.JPasswordField();
        jLabel3 = new javax.swing.JLabel();
        cbGrupo = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        txtData = new javax.swing.JFormattedTextField();
        btnAtualiza = new javax.swing.JButton();

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(472, 442));
        setResizable(false);

        jToolBar1.setRollover(true);

        btnIncluir.setText("Incluir");
        btnIncluir.setFocusable(false);
        btnIncluir.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnIncluir.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnIncluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIncluirActionPerformed(evt);
            }
        });
        jToolBar1.add(btnIncluir);

        btnAlterar.setText("Alterar");
        btnAlterar.setFocusable(false);
        btnAlterar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnAlterar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnAlterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAlterarActionPerformed(evt);
            }
        });
        jToolBar1.add(btnAlterar);

        btnExcluir.setText("Excluir");
        btnExcluir.setFocusable(false);
        btnExcluir.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnExcluir.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcluirActionPerformed(evt);
            }
        });
        jToolBar1.add(btnExcluir);

        btnGravar.setText("Gravar");
        btnGravar.setFocusable(false);
        btnGravar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnGravar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnGravar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGravarActionPerformed(evt);
            }
        });
        jToolBar1.add(btnGravar);
        jToolBar1.add(filler1);

        btnCancelar.setText("Cancelar");
        btnCancelar.setFocusable(false);
        btnCancelar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnCancelar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        jToolBar1.add(btnCancelar);

        tbUsuarios.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Código", "Login", "Grupo", "Data Expiração"
            }
        ));
        jScrollPane2.setViewportView(tbUsuarios);
        if (tbUsuarios.getColumnModel().getColumnCount() > 0) {
            tbUsuarios.getColumnModel().getColumn(3).setCellRenderer(null);
        }

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Usuario"));

        jLabel1.setText("Login:");

        jLabel2.setText("Pass:");

        txtPass.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPassActionPerformed(evt);
            }
        });

        jLabel3.setText("Grupo:");

        cbGrupo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cbGrupo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbGrupoActionPerformed(evt);
            }
        });

        jLabel4.setText("Expiração:");

        txtData.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter()));
        txtData.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        btnAtualiza.setText("Atualizar");
        btnAtualiza.setFocusable(false);
        btnAtualiza.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnAtualiza.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnAtualiza.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAtualizaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3))
                        .addGap(41, 41, 41)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtPass, javax.swing.GroupLayout.DEFAULT_SIZE, 355, Short.MAX_VALUE)
                            .addComponent(txtLogin)
                            .addComponent(cbGrupo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(18, 18, 18)
                        .addComponent(txtData, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnAtualiza)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtLogin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtPass, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(cbGrupo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel4)
                        .addComponent(txtData, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnAtualiza, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 462, Short.MAX_VALUE)))
                    .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnGravarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGravarActionPerformed
        gravar();
        limpaCampos();
        configuarTela(0);
        
    }//GEN-LAST:event_btnGravarActionPerformed

    private void btnIncluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIncluirActionPerformed
        configuarTela(1);
        limpaCampos();
    }//GEN-LAST:event_btnIncluirActionPerformed

    private void cbGrupoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbGrupoActionPerformed
        System.out.println("plong");

    }//GEN-LAST:event_cbGrupoActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        configuarTela(0);
        limpaCampos();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAlterarActionPerformed
        configuarTela(0);
        alterar();
        verifica = false;
    }//GEN-LAST:event_btnAlterarActionPerformed

    private void btnAtualizaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAtualizaActionPerformed
        buscarDados();
        configuarTela(0);
        limpaCampos();
    }//GEN-LAST:event_btnAtualizaActionPerformed

    private void btnExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcluirActionPerformed
        excluir();
        configuarTela(0);
        limpaCampos();
    }//GEN-LAST:event_btnExcluirActionPerformed

    private void txtPassActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPassActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPassActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ManutencaoUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ManutencaoUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ManutencaoUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ManutencaoUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ManutencaoUsuario().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAlterar;
    private javax.swing.JButton btnAtualiza;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnExcluir;
    private javax.swing.JButton btnGravar;
    private javax.swing.JButton btnIncluir;
    private javax.swing.JComboBox<String> cbGrupo;
    private javax.swing.Box.Filler filler1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JTable tbUsuarios;
    private javax.swing.JFormattedTextField txtData;
    private javax.swing.JTextField txtLogin;
    private javax.swing.JPasswordField txtPass;
    // End of variables declaration//GEN-END:variables
}
