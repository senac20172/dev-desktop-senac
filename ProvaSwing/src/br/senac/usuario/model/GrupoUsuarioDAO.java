/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.senac.usuario.model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class GrupoUsuarioDAO {
    
    
    private static ArrayList<GrupoUsuario> listaGrupoUsuario;
    
    public GrupoUsuarioDAO(){
        
        if(listaGrupoUsuario == null){
            listaGrupoUsuario = new ArrayList<>();
            listaGrupoUsuario.add(new GrupoUsuario(1, "Administrador"));
            listaGrupoUsuario.add(new GrupoUsuario(2, "Recursos Humanos"));
            listaGrupoUsuario.add(new GrupoUsuario(3, "Faturamento"));
            listaGrupoUsuario.add(new GrupoUsuario(4, "Compras"));
            listaGrupoUsuario.add(new GrupoUsuario(5, "Vendas"));
            listaGrupoUsuario.add(new GrupoUsuario(6, "Contabilidade"));
            listaGrupoUsuario.add(new GrupoUsuario(7, "Manufatura"));
            listaGrupoUsuario.add(new GrupoUsuario(8, "Estoque"));
            listaGrupoUsuario.add(new GrupoUsuario(9, "Frente Caixa"));
            listaGrupoUsuario.add(new GrupoUsuario(10, "Contas a Pagar"));
            listaGrupoUsuario.add(new GrupoUsuario(11, "Contas a Receber"));
        }
        
    }
    
    public List<GrupoUsuario> listarTodos(){
        return listaGrupoUsuario;
    }
    
    public GrupoUsuario getPorId(Integer id){
        for(GrupoUsuario grupoUsuario : listaGrupoUsuario){
            if(grupoUsuario.getIdGrupoUsuario().equals(id)){
                return grupoUsuario;
            }
        }
        return null;
    }
    
}
